# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kdeconnect-kde package.
#
# Stelios <sstavra@gmail.com>, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: kdeconnect-kde\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-12 00:45+0000\n"
"PO-Revision-Date: 2021-06-19 07:51+0300\n"
"Last-Translator: Stelios <sstavra@gmail.com>\n"
"Language-Team: Greek <kde-i18n-el@kde.org>\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.04.2\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Stelios"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "sstavra@gmail.com"

#: deviceindicator.cpp:53
#, kde-format
msgid "Browse device"
msgstr "Περιήγηση συσκευής"

#: deviceindicator.cpp:67
#, kde-format
msgid "Ring device"
msgstr "Ήχος συσκευής"

#: deviceindicator.cpp:81
#, kde-format
msgid "Get a photo"
msgstr "Πάρτε μια φωτογραφία"

#: deviceindicator.cpp:101
#, kde-format
msgid "Send a file/URL"
msgstr "Αποστολή αρχείου/URL"

#: deviceindicator.cpp:111
#, kde-format
msgid "SMS Messages..."
msgstr "SMS μηνύματα..."

#: deviceindicator.cpp:124
#, kde-format
msgid "Run command"
msgstr "Εκτέλεση εντολής"

#: deviceindicator.cpp:126
#, kde-format
msgid "Add commands"
msgstr "Προσθήκη εντολών"

#: indicatorhelper_mac.cpp:34
#, kde-format
msgid "Launching"
msgstr "Εκτέλεση"

#: indicatorhelper_mac.cpp:84
#, kde-format
msgid "Launching daemon"
msgstr "Εκτέλεση δαίμονα"

#: indicatorhelper_mac.cpp:93
#, kde-format
msgid "Waiting D-Bus"
msgstr "Αναμονή για D-Bus"

#: indicatorhelper_mac.cpp:110 indicatorhelper_mac.cpp:120
#: indicatorhelper_mac.cpp:135
#, kde-format
msgid "KDE Connect"
msgstr "KDE Connect"

#: indicatorhelper_mac.cpp:111 indicatorhelper_mac.cpp:121
#, kde-format
msgid ""
"Cannot connect to DBus\n"
"KDE Connect will quit"
msgstr ""
"Αδυναμία σύνδεσης στο DBus\n"
"Το KDE Connect θα τερματιστεί"

#: indicatorhelper_mac.cpp:135
#, kde-format
msgid "Cannot find kdeconnectd"
msgstr "Αδυναμία εύρεσης του kdeconnectd"

#: indicatorhelper_mac.cpp:140
#, kde-format
msgid "Loading modules"
msgstr "Φόρτωση αρθρωμάτων"

#: main.cpp:41
#, kde-format
msgid "KDE Connect Indicator"
msgstr "Ένδειξη του KDE Connect"

#: main.cpp:43
#, kde-format
msgid "KDE Connect Indicator tool"
msgstr "Εργαλείο ένδειξης του KDE Connect"

#: main.cpp:45
#, kde-format
msgid "(C) 2016 Aleix Pol Gonzalez"
msgstr "(C) 2016 Aleix Pol Gonzalez"

#: main.cpp:80
#, kde-format
msgid "Configure..."
msgstr "Διαμόρφωση..."

#: main.cpp:102
#, kde-format
msgid "Pairing requests"
msgstr "Αιτήσεις σύζευξης"

#: main.cpp:107
#, kde-format
msgid "Pair"
msgstr "Ζεύγος"

#: main.cpp:108
#, kde-format
msgid "Reject"
msgstr "Απόρριψη"

#: main.cpp:114 main.cpp:124
#, kde-format
msgid "Quit"
msgstr "Έξοδος"

#: main.cpp:143 main.cpp:167
#, kde-format
msgid "%1 device connected"
msgid_plural "%1 devices connected"
msgstr[0] "%1 συσκευή συνδεδεμένη"
msgstr[1] "%1 συσκευές συνδεδεμένες"

#: systray_actions/battery_action.cpp:28
#, kde-format
msgid "No Battery"
msgstr "Δεν υπάρχει μπαταρία"

#: systray_actions/battery_action.cpp:30
#, kde-format
msgid "Battery: %1% (Charging)"
msgstr "Μπαταρία: %1% (Φορτίζει)"

#: systray_actions/battery_action.cpp:32
#, kde-format
msgid "Battery: %1%"
msgstr "Μπαταρία: %1%"

#: systray_actions/connectivity_action.cpp:30
#, kde-format
msgctxt ""
"The fallback text to display in case the remote device does not have a "
"cellular connection"
msgid "No Cellular Connectivity"
msgstr "Δεν υπάρχει κυψελωτή δυνδεσιμότητα"

#: systray_actions/connectivity_action.cpp:33
#, kde-format
msgctxt ""
"Display the cellular connection type and an approximate percentage of signal "
"strength"
msgid "%1  | ~%2%"
msgstr "%1  | ~%2%"

#~ msgid "Select file to send to '%1'"
#~ msgstr "Επιλογή αρχείου για αποστολή στο '%1'"

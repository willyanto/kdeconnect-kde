# Translation of kdeconnect-core.po to Brazilian Portuguese
# Copyright (C) 2014-2019 This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# André Marcelo Alvarenga <alvarenga@kde.org>, 2014, 2015, 2016, 2018, 2019.
# Luiz Fernando Ranghetti <elchevive@opensuse.org>, 2015, 2016, 2019, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: kdeconnect-core\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-12 00:45+0000\n"
"PO-Revision-Date: 2021-02-01 16:26-0300\n"
"Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>\n"
"Language-Team: Brazilian Portuguese <kde-i18n-pt_BR@kde.org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 20.04.3\n"

#: backends/bluetooth/bluetoothpairinghandler.cpp:56
#: backends/lan/lanpairinghandler.cpp:52
#, kde-format
msgid "Canceled by other peer"
msgstr "Cancelado pelo outro dispositivo"

#: backends/bluetooth/bluetoothpairinghandler.cpp:65
#: backends/lan/lanpairinghandler.cpp:61
#, kde-format
msgid "%1: Already paired"
msgstr "%1: Já emparelhado"

#: backends/bluetooth/bluetoothpairinghandler.cpp:68
#, kde-format
msgid "%1: Pairing already requested for this device"
msgstr "%1: Já foi solicitado o emparelhamento deste dispositivo"

#: backends/bluetooth/bluetoothpairinghandler.cpp:124
#: backends/lan/lanpairinghandler.cpp:106
#, kde-format
msgid "Timed out"
msgstr "Tempo limite expirou"

#: backends/lan/compositeuploadjob.cpp:82
#, kde-format
msgid "Couldn't find an available port"
msgstr "Não foi possível encontrar uma porta disponível"

#: backends/lan/compositeuploadjob.cpp:121
#, kde-format
msgid "Failed to send packet to %1"
msgstr "Ocorreu uma falha ao enviar o pacote para %1"

#: backends/lan/compositeuploadjob.cpp:287
#, kde-format
msgid "Sending to %1"
msgstr "Enviando para %1"

#: backends/lan/compositeuploadjob.cpp:287
#, kde-format
msgid "File"
msgstr "Arquivo"

#: backends/lan/landevicelink.cpp:146 backends/lan/landevicelink.cpp:160
#, kde-format
msgid ""
"This device cannot be paired because it is running an old version of KDE "
"Connect."
msgstr ""
"Este dispositivo não pode ser emparelhado por estar executando uma versão "
"antiga do KDE Connect."

#: compositefiletransferjob.cpp:46
#, kde-format
msgctxt "@title job"
msgid "Receiving file"
msgid_plural "Receiving files"
msgstr[0] "Recebendo arquivo"
msgstr[1] "Recebendo arquivos"

#: compositefiletransferjob.cpp:47
#, kde-format
msgctxt "The source of a file operation"
msgid "Source"
msgstr "Origem"

#: compositefiletransferjob.cpp:48
#, kde-format
msgctxt "The destination of a file operation"
msgid "Destination"
msgstr "Destino"

#: device.cpp:215
#, kde-format
msgid "Already paired"
msgstr "Já emparelhado"

#: device.cpp:220
#, kde-format
msgid "Device not reachable"
msgstr "Dispositivo inacessível"

#: device.cpp:561
#, kde-format
msgid "SHA256 fingerprint of your device certificate is: %1\n"
msgstr "A impressão digital SHA256 do certificado do seu dispositivo é: %1\n"

#: device.cpp:569
#, kde-format
msgid "SHA256 fingerprint of remote device certificate is: %1\n"
msgstr ""
"A impressão digital SHA256 do certificado do dispositivo remoto é: %1\n"

#: filetransferjob.cpp:51
#, kde-format
msgid "Filename already present"
msgstr "O nome de arquivo já está presente"

#: filetransferjob.cpp:100
#, kde-format
msgid "Received incomplete file: %1"
msgstr "Arquivo incompleto recebido: %1"

#: filetransferjob.cpp:118
#, kde-format
msgid "Received incomplete file from: %1"
msgstr "Arquivo incompleto recebido de: %1"

#: kdeconnectconfig.cpp:76
#, kde-format
msgid "KDE Connect failed to start"
msgstr "Falha ao iniciar o KDE Connect"

#: kdeconnectconfig.cpp:77
#, kde-format
msgid ""
"Could not find support for RSA in your QCA installation. If your "
"distribution provides separate packets for QCA-ossl and QCA-gnupg, make sure "
"you have them installed and try again."
msgstr ""
"Não foi encontrado suporte para RSA na sua instalação do QCA. Se a sua "
"distribuição fornece pacotes separados para QCA-ossl e QCA-gnupg, verifique "
"se você tem ambos instalados e tente novamente."

#: kdeconnectconfig.cpp:313
#, kde-format
msgid "Could not store private key file: %1"
msgstr "Não foi possível armazenar o arquivo de chave privada: %1"

#: kdeconnectconfig.cpp:358
#, kde-format
msgid "Could not store certificate file: %1"
msgstr "Não foi possível armazenar o arquivo do certificado: %1"

#~ msgid "Sent 1 file"
#~ msgid_plural "Sent %1 files"
#~ msgstr[0] "Enviar 1 arquivo"
#~ msgstr[1] "Enviar %1 arquivos"

#~ msgid "Progress"
#~ msgstr "Progresso"

#~ msgid "Sending file %1 of %2"
#~ msgstr "Enviando arquivo %1 de %2"

#~ msgid "Receiving file %1 of %2"
#~ msgstr "Recebendo arquivo %1 de %2"

#~ msgid "Receiving file over KDE Connect"
#~ msgstr "Recebendo arquivo através do KDE Connect"

#~ msgctxt "File transfer origin"
#~ msgid "From"
#~ msgstr "De"

#~ msgctxt "File transfer destination"
#~ msgid "To"
#~ msgstr "Para"

#~ msgid "Finished sending to %1"
#~ msgstr "Terminou o envio para %1"
